import mongoose from 'mongoose';
import constants from './config/constants';

// Use native promises
mongoose.Promise = global.Promise;

// Connect to database;
mongoose.connect(constants.DB_URL_DEV);
mongoose.connection.on('error', err => {
  throw err;
});
