import { Router } from 'express';
import ApplicationsController from './controllers/application.controller';

const routes = new Router();

// Applications
routes.post('/applications', ApplicationsController.create);
routes.get('/applications', ApplicationsController.getAll);

export default routes;
