import express from 'express';
import bodyParser from 'body-parser';
import routes from './routes';
import constants from './config/constants';

const app = express();

// Parse requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Mount routes
app.use('/api', routes);

app.listen(constants.PORT, () => {
  console.log(`
    Everything works!
    Port: ${constants.PORT}
    Env: ${app.get('env')}
  `);
});

export default app;
