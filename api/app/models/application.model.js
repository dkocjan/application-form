import mongoose from 'mongoose';
import { isEmail } from 'validator';

const { Schema } = mongoose;

const ApplicationSchema = new Schema(
  {
    name: {
      type: String,
      capitalized: true,
      required: 'Name is required',
    },
    lastname: {
      type: String,
      capitalized: true,
      required: 'Last name is required',
    },
    email: {
      type: String,
      trim: true,
      lowercase: true,
      unique: true,
      required: 'Email is required',
      validate: {
        validator: isEmail,
        message: '{VALUE} is not a valid email',
        isAsync: false,
      },
    },
    date: {
      type: Date,
      required: 'Date is required',
    },
  },
  {
    timestamps: true,
  }
);

const ApplicationModel = mongoose.model('Application', ApplicationSchema);

export default ApplicationModel;
