const constants = {
  PORT: 3001,
  DB_URL_DEV: 'mongodb://brainhub:123@ds119070.mlab.com:19070/brainhub',
  DB_URL_TEST: 'mongodb://brainhub:123@ds219000.mlab.com:19000/brainhub-test',
};

export default constants;
