import Application from '../models/application.model';

class ApplicationController {
  create = async (req, res, next) => {
    const params = req.body;

    const application = new Application({
      ...params,
    });

    try {
      res.status(201).json(await application.save());
    } catch (err) {
      next(err);
    }
  };

  getAll = async (req, res, next) => {
    try {
      const applications = await Application.find({});

      res.json(applications);
    } catch (err) {
      next(err);
    }
  };
}

export default new ApplicationController();
