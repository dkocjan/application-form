import React, { Component } from 'react';
import { Menu, Icon } from 'semantic-ui-react';

class MyComponent extends Component {
  state = {
    activeItem: 'application',
  };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    const { activeItem } = this.state;

    return (
      <Menu stackable>
        <Menu.Item
          name="application"
          active={activeItem === 'application'}
          onClick={this.handleItemClick}
        >
          <Icon circular inverted color="teal" name="wpforms" />
          Application Form
        </Menu.Item>
      </Menu>
    );
  }
}

export default MyComponent;
