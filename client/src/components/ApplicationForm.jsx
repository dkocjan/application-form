import React, { PureComponent } from 'react';
import { Button, Form } from 'semantic-ui-react';

class ApplicationForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Form>
        <Form.Group widths="equal">
          <Form.Field>
            <label htmlFor="application-first-name">
              First Name
              <input
                placeholder="First Name"
                id="application-first-name"
                type="text"
              />
            </label>
          </Form.Field>
          <Form.Field>
            <label htmlFor="application-last-name">
              Last Name
              <input
                id="application-last-name"
                placeholder="Last Name"
                type="text"
              />
            </label>
          </Form.Field>
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Field>
            <label htmlFor="application-email">
              Email
              <input id="application-email" placeholder="E-mail" type="email" />
            </label>
          </Form.Field>
          <Form.Field>
            <label htmlFor="application-date">
              Date
              <input id="application-date" type="date" />
            </label>
          </Form.Field>
        </Form.Group>
        <Button type="submit">Submit</Button>
      </Form>
    );
  }
}

ApplicationForm.propTypes = {};

export default ApplicationForm;
