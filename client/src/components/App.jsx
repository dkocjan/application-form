import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
import { Container } from 'semantic-ui-react';
import ApplicationForm from './ApplicationForm';
import Menu from './Menu';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container>
        <Menu />
        <ApplicationForm />
      </Container>
    );
  }
}

export default App;
